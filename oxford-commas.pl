#!/usr/bin/perl
# Take a list of words or phrases separated by ", " and add "and " before the last item.

use strict;
use warnings;

use autodie;
use Text::CSV;

my $csv = Text::CSV->new ( { binary => 1 } )  # should set binary attribute.
                or die "Cannot use CSV: ".Text::CSV->error_diag ();

open ( my $fh_in, "<", "test.csv" );
open ( my $fh_out, ">", "test-out.csv" );

while ( my $row = $csv->getline( $fh_in ) ) {
    $csv->eol ("\r\n");
    my $id = $row->[0];
    my $text = $row->[1];
    my $text_new = $text;
    my $count = $row->[2];
    if ($count > 2) {
        my @text_split = split(/, /, $text);
        $text_split[-1] = "and " . $text_split[-1];
        $text_new = join(", ", @text_split);
    }
    elsif ($count == 2) {
        $text_new =~ s/, / and / ;
    }
    my @row_out = ($id, $text_new, $count);
    my $row_out_ref = \@row_out;
    $csv->print($fh_out, $row_out_ref);
}

$csv->eof or $csv->error_diag();
close $fh_in;
close $fh_out;

1;

